# Manomotion Trigger gesture

## Changing the color of the cube using hand gesture

## Preview:
![alt text](https://gitlab.com/rahulmool/interactable_cube/-/raw/master/readmedata/img1.jpeg)
![alt text](https://gitlab.com/rahulmool/interactable_cube/-/raw/master/readmedata/img2.jpeg)

[Testing Video](https://gitlab.com/rahulmool/interactable_cube/-/raw/master/readmedata/video.mp4)

## Tutorial Link:
[![IMAGE ALT TEXT HERE](https://i.ytimg.com/vi/NLCN1ondCxM/maxresdefault.jpg)](https://youtu.be/NLCN1ondCxM)
[https://youtu.be/NLCN1ondCxM](https://youtu.be/NLCN1ondCxM)
