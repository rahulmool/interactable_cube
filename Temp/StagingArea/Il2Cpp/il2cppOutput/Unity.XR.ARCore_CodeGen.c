﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* Provider_CameraPermissionRequestProvider_mFEC487B73642789DA07336909087B919BDADF1EB_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.XR.ARCore.ARCoreCameraSubsystem::Register()
extern void ARCoreCameraSubsystem_Register_mDDF7E16C611351F9BC5D925C8B536CDE3F90F95D (void);
// 0x00000002 UnityEngine.XR.ARSubsystems.XRCameraSubsystem_IProvider UnityEngine.XR.ARCore.ARCoreCameraSubsystem::CreateProvider()
extern void ARCoreCameraSubsystem_CreateProvider_m37D3B9E5E9381A4557973747A59F56F495D1A665 (void);
// 0x00000003 System.Void UnityEngine.XR.ARCore.ARCoreCameraSubsystem::.ctor()
extern void ARCoreCameraSubsystem__ctor_mE51F011BF37C360CBDFB4CA00DD190AF271329E5 (void);
// 0x00000004 System.Void UnityEngine.XR.ARCore.ARCoreCameraSubsystem_Provider::.ctor()
extern void Provider__ctor_m4FE465C11F9431414310E3A4A8F301725BC94A44 (void);
// 0x00000005 System.Void UnityEngine.XR.ARCore.ARCoreCameraSubsystem_Provider::.cctor()
extern void Provider__cctor_mF801B25F3D1AE886BE57B6217EF9EF88A28B6F16 (void);
// 0x00000006 System.Void UnityEngine.XR.ARCore.ARCoreCameraSubsystem_NativeApi::UnityARCore_Camera_Construct(System.Int32)
extern void NativeApi_UnityARCore_Camera_Construct_m79D86E9F4D36A18B6B06A8E3E323055D39EE02AB (void);
// 0x00000007 System.Boolean UnityEngine.XR.ARCore.ARCoreFaceRegionData::Equals(UnityEngine.XR.ARCore.ARCoreFaceRegionData)
extern void ARCoreFaceRegionData_Equals_m7462EF32B68A814216D80B6F22CA38BBCFB8737A_AdjustorThunk (void);
// 0x00000008 System.Int32 UnityEngine.XR.ARCore.ARCoreFaceRegionData::GetHashCode()
extern void ARCoreFaceRegionData_GetHashCode_mCA630AF2258A0C864182839D6B41B320F8ED9DC0_AdjustorThunk (void);
// 0x00000009 System.Boolean UnityEngine.XR.ARCore.ARCoreFaceRegionData::Equals(System.Object)
extern void ARCoreFaceRegionData_Equals_mFFB1945FFCB1020D86F83DB2060667DBC90A830D_AdjustorThunk (void);
// 0x0000000A System.String UnityEngine.XR.ARCore.ARCoreFaceRegionData::ToString()
extern void ARCoreFaceRegionData_ToString_m039CBE0D635D1C6C4E704C58BC363D3E2394E7FE_AdjustorThunk (void);
// 0x0000000B UnityEngine.XR.ARSubsystems.XRFaceSubsystem_IProvider UnityEngine.XR.ARCore.ARCoreFaceSubsystem::CreateProvider()
extern void ARCoreFaceSubsystem_CreateProvider_m0051EDA00E80118FC9650938DF8464B868E443D8 (void);
// 0x0000000C System.Void UnityEngine.XR.ARCore.ARCoreFaceSubsystem::RegisterDescriptor()
extern void ARCoreFaceSubsystem_RegisterDescriptor_mA6CB48575E3B51FBF4D715631EEB9DF7B2C418F8 (void);
// 0x0000000D System.Void UnityEngine.XR.ARCore.ARCoreFaceSubsystem::.ctor()
extern void ARCoreFaceSubsystem__ctor_m8DE7034F95A61AEE431FE4FB9C854DBB9AA7B7C2 (void);
// 0x0000000E System.Void UnityEngine.XR.ARCore.ARCoreFaceSubsystem_Provider::.ctor()
extern void Provider__ctor_m5255950E187F6D5E7078B7D791567C086AC1DD27 (void);
// 0x0000000F System.Void UnityEngine.XR.ARCore.ARCoreImageTrackingProvider::RegisterDescriptor()
extern void ARCoreImageTrackingProvider_RegisterDescriptor_mA50E29E6F9F09ADF1254521E2BFF433D1D21980B (void);
// 0x00000010 UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem_IProvider UnityEngine.XR.ARCore.ARCoreImageTrackingProvider::CreateProvider()
extern void ARCoreImageTrackingProvider_CreateProvider_m929BC6984DEBB3271FE73D4355631A5CD4FE3143 (void);
// 0x00000011 System.Void UnityEngine.XR.ARCore.ARCoreImageTrackingProvider::.ctor()
extern void ARCoreImageTrackingProvider__ctor_m17C6EDEEB185D756FEEB5C4A6AB5EC6FB513D18E (void);
// 0x00000012 System.Void UnityEngine.XR.ARCore.ARCoreImageTrackingProvider::.cctor()
extern void ARCoreImageTrackingProvider__cctor_m0DB6BF0658862E8DE668A7C101966D3224A76F0C (void);
// 0x00000013 System.Void UnityEngine.XR.ARCore.ARCoreImageTrackingProvider_Provider::.ctor()
extern void Provider__ctor_m1AA2B9F3133A6B60C36B687171E4124445D0A8CE (void);
// 0x00000014 System.Boolean UnityEngine.XR.ARCore.ARCorePermissionManager::IsPermissionGranted(System.String)
extern void ARCorePermissionManager_IsPermissionGranted_m06192CF52A7FC87F26C35620EBF07B7E745B0873 (void);
// 0x00000015 System.Void UnityEngine.XR.ARCore.ARCorePermissionManager::RequestPermission(System.String,System.Action`2<System.String,System.Boolean>)
extern void ARCorePermissionManager_RequestPermission_mE4D15DE1B58505ACE2F3FC50789DD35FCDCC22B2 (void);
// 0x00000016 System.Void UnityEngine.XR.ARCore.ARCorePermissionManager::OnPermissionGranted(System.String)
extern void ARCorePermissionManager_OnPermissionGranted_m4AEF9C4D1F4A87269AC1B452C33209BBC6128940 (void);
// 0x00000017 System.Void UnityEngine.XR.ARCore.ARCorePermissionManager::OnPermissionDenied(System.String)
extern void ARCorePermissionManager_OnPermissionDenied_m80AB1413F67AA69B3AFF5C0C51A2B1F5BB3353FB (void);
// 0x00000018 System.Void UnityEngine.XR.ARCore.ARCorePermissionManager::OnActivityResult()
extern void ARCorePermissionManager_OnActivityResult_m441A5092D116700AFDF601F625550CFDDEB34495 (void);
// 0x00000019 System.Void UnityEngine.XR.ARCore.ARCorePermissionManager::.ctor()
extern void ARCorePermissionManager__ctor_m3525133DF4FDE7359B1AF77878DCBE7D66867735 (void);
// 0x0000001A UnityEngine.XR.ARCore.ARCorePermissionManager UnityEngine.XR.ARCore.ARCorePermissionManager::get_instance()
extern void ARCorePermissionManager_get_instance_mCBE338C3C114B3F52E2EC1615992F294952133B5 (void);
// 0x0000001B UnityEngine.AndroidJavaObject UnityEngine.XR.ARCore.ARCorePermissionManager::get_activity()
extern void ARCorePermissionManager_get_activity_m69909D383ADADC460EA1BCE389ADA0712F69E004 (void);
// 0x0000001C UnityEngine.AndroidJavaObject UnityEngine.XR.ARCore.ARCorePermissionManager::get_permissionsService()
extern void ARCorePermissionManager_get_permissionsService_m48CB41AA73C9AAAADF94C0C0A7EFD8442A38AEF3 (void);
// 0x0000001D System.Void UnityEngine.XR.ARCore.ARCorePermissionManager::.cctor()
extern void ARCorePermissionManager__cctor_mFD32765583188B71172CCBC3BA44F25BFE37B383 (void);
// 0x0000001E UnityEngine.XR.ARSubsystems.XRPlaneSubsystem_IProvider UnityEngine.XR.ARCore.ARCorePlaneProvider::CreateProvider()
extern void ARCorePlaneProvider_CreateProvider_mA8C70639859973A8D0F8FD8CD54CD4F2BDD8C08B (void);
// 0x0000001F System.Void UnityEngine.XR.ARCore.ARCorePlaneProvider::RegisterDescriptor()
extern void ARCorePlaneProvider_RegisterDescriptor_m83AC8796A906FB87A486CE712EEFD886CBE472C8 (void);
// 0x00000020 System.Void UnityEngine.XR.ARCore.ARCorePlaneProvider::.ctor()
extern void ARCorePlaneProvider__ctor_mFAAB1DD1BD53A1B01F26D3F7023C19F8DEA3D97F (void);
// 0x00000021 System.Void UnityEngine.XR.ARCore.ARCorePlaneProvider_Provider::.ctor()
extern void Provider__ctor_m72BD3537B4C02847A8CF8CC55E03EE3F50802A4F (void);
// 0x00000022 UnityEngine.XR.ARSubsystems.XRRaycastSubsystem_IProvider UnityEngine.XR.ARCore.ARCoreRaycastSubsystem::CreateProvider()
extern void ARCoreRaycastSubsystem_CreateProvider_m5FE3039D0AB6C39BCC9468186FC5739BEF9FF262 (void);
// 0x00000023 System.Void UnityEngine.XR.ARCore.ARCoreRaycastSubsystem::RegisterDescriptor()
extern void ARCoreRaycastSubsystem_RegisterDescriptor_m9C14ADD8BB24C067D22A1F7F34C0A24A55F9F565 (void);
// 0x00000024 System.Void UnityEngine.XR.ARCore.ARCoreRaycastSubsystem::.ctor()
extern void ARCoreRaycastSubsystem__ctor_m843D27D516C0C39D4E3DF17CB4A2C5132B902A1B (void);
// 0x00000025 System.Void UnityEngine.XR.ARCore.ARCoreRaycastSubsystem_Provider::.ctor()
extern void Provider__ctor_m07868EB509C138E07663A12E5DAC6DA3D8D79FCF (void);
// 0x00000026 UnityEngine.XR.ARSubsystems.XRReferencePointSubsystem_IProvider UnityEngine.XR.ARCore.ARCoreReferencePointSubsystem::CreateProvider()
extern void ARCoreReferencePointSubsystem_CreateProvider_mE42AA83FA06ED73CA48431E23913EFF6B9AC7E80 (void);
// 0x00000027 System.Void UnityEngine.XR.ARCore.ARCoreReferencePointSubsystem::RegisterDescriptor()
extern void ARCoreReferencePointSubsystem_RegisterDescriptor_m5BEEAD4CFCB58AECC5E879920CF10E694466A2B0 (void);
// 0x00000028 System.Void UnityEngine.XR.ARCore.ARCoreReferencePointSubsystem::.ctor()
extern void ARCoreReferencePointSubsystem__ctor_m292D29A04B8FD6BA5C86E6326198EFA13BA9397A (void);
// 0x00000029 System.Void UnityEngine.XR.ARCore.ARCoreReferencePointSubsystem_Provider::.ctor()
extern void Provider__ctor_mE4F3BDB17D6AE41525A793DB7B4B0E21981B6633 (void);
// 0x0000002A UnityEngine.XR.ARSubsystems.XRSessionSubsystem_IProvider UnityEngine.XR.ARCore.ARCoreSessionSubsystem::CreateProvider()
extern void ARCoreSessionSubsystem_CreateProvider_m3E2A8BA6B3C5BB07A6AE4B21834F229AE70745A7 (void);
// 0x0000002B System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem::RegisterDescriptor()
extern void ARCoreSessionSubsystem_RegisterDescriptor_mD495549DAA1F4E1765DD584E3A5F34045448787E (void);
// 0x0000002C System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem::.ctor()
extern void ARCoreSessionSubsystem__ctor_m2200AD4AACCBC343C9D048A42515CBCB448777EA (void);
// 0x0000002D System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem_Provider::.ctor(UnityEngine.XR.ARCore.ARCoreSessionSubsystem)
extern void Provider__ctor_mC9DABA5114D6E7ABB3122F65D7DA76209EF8DBEC (void);
// 0x0000002E System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem_Provider::CameraPermissionRequestProvider(UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi_CameraPermissionsResultCallbackDelegate,System.IntPtr)
extern void Provider_CameraPermissionRequestProvider_mFEC487B73642789DA07336909087B919BDADF1EB (void);
// 0x0000002F System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem_Provider_<>c::.cctor()
extern void U3CU3Ec__cctor_m64E09ED95E8C425402E9B2DD8C8634E162E62E53 (void);
// 0x00000030 System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem_Provider_<>c::.ctor()
extern void U3CU3Ec__ctor_mA0A6C55D43B524BE30189C46201EBFD3540B8C07 (void);
// 0x00000031 System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem_Provider_<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m5B8CADE5FA4F4F1927C780A7E21000AC3F8F0F80 (void);
// 0x00000032 System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem_Provider_<>c__DisplayClass25_0::<CameraPermissionRequestProvider>b__0(System.String,System.Boolean)
extern void U3CU3Ec__DisplayClass25_0_U3CCameraPermissionRequestProviderU3Eb__0_m52F0786879125E6E658440ED03143240D68D2522 (void);
// 0x00000033 System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi::UnityARCore_session_construct(UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi_CameraPermissionRequestProviderDelegate)
extern void NativeApi_UnityARCore_session_construct_mB49ED374A1D21F55A8BB6F1B2E24473E4AE1F69E (void);
// 0x00000034 System.IntPtr UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi::UnityARCore_session_getRenderEventFunc()
extern void NativeApi_UnityARCore_session_getRenderEventFunc_mED022295B10F6DF463E1CF02A5BD5576A955B831 (void);
// 0x00000035 System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi_CameraPermissionRequestProviderDelegate::.ctor(System.Object,System.IntPtr)
extern void CameraPermissionRequestProviderDelegate__ctor_m792EC0BE8D37B67EF22A0F0667E07C2D1CFD3F0A (void);
// 0x00000036 System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi_CameraPermissionRequestProviderDelegate::Invoke(UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi_CameraPermissionsResultCallbackDelegate,System.IntPtr)
extern void CameraPermissionRequestProviderDelegate_Invoke_m54960A3B1E623735752A53AF5603BFC7F701A049 (void);
// 0x00000037 System.IAsyncResult UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi_CameraPermissionRequestProviderDelegate::BeginInvoke(UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi_CameraPermissionsResultCallbackDelegate,System.IntPtr,System.AsyncCallback,System.Object)
extern void CameraPermissionRequestProviderDelegate_BeginInvoke_mDCF49DB5139A48727879CBC13286D0974D12C698 (void);
// 0x00000038 System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi_CameraPermissionRequestProviderDelegate::EndInvoke(System.IAsyncResult)
extern void CameraPermissionRequestProviderDelegate_EndInvoke_m3DAFBBEAB7D5A85A049FC5A2C77749E65DE06338 (void);
// 0x00000039 System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi_CameraPermissionsResultCallbackDelegate::.ctor(System.Object,System.IntPtr)
extern void CameraPermissionsResultCallbackDelegate__ctor_m5EAA78ACA7411CB386F621E6AB23D2242C3DB770 (void);
// 0x0000003A System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi_CameraPermissionsResultCallbackDelegate::Invoke(System.Boolean,System.IntPtr)
extern void CameraPermissionsResultCallbackDelegate_Invoke_mBDC09109E4DC2472D4505500737052AA9132641F (void);
// 0x0000003B System.IAsyncResult UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi_CameraPermissionsResultCallbackDelegate::BeginInvoke(System.Boolean,System.IntPtr,System.AsyncCallback,System.Object)
extern void CameraPermissionsResultCallbackDelegate_BeginInvoke_mC45147E135C1EC1C2179503348F715FFD659EC2C (void);
// 0x0000003C System.Void UnityEngine.XR.ARCore.ARCoreSessionSubsystem_NativeApi_CameraPermissionsResultCallbackDelegate::EndInvoke(System.IAsyncResult)
extern void CameraPermissionsResultCallbackDelegate_EndInvoke_mC0B26AC474FC40ACBA09400904A4E70D8D41F272 (void);
// 0x0000003D UnityEngine.XR.ARSubsystems.XRDepthSubsystem_IDepthApi UnityEngine.XR.ARCore.ARCoreXRDepthSubsystem::GetInterface()
extern void ARCoreXRDepthSubsystem_GetInterface_m1964AA9C2CA4E61B72FEB25D0E292DADA9E7C378 (void);
// 0x0000003E System.Void UnityEngine.XR.ARCore.ARCoreXRDepthSubsystem::RegisterDescriptor()
extern void ARCoreXRDepthSubsystem_RegisterDescriptor_m58189C773B57886BF80869AE81ED9477E7E84C21 (void);
// 0x0000003F System.Void UnityEngine.XR.ARCore.ARCoreXRDepthSubsystem::.ctor()
extern void ARCoreXRDepthSubsystem__ctor_mDFCC3AF83D30FA2DBCFE2AB46C09617892462886 (void);
// 0x00000040 System.Void UnityEngine.XR.ARCore.ARCoreXRDepthSubsystem_Provider::.ctor()
extern void Provider__ctor_m6EFC1759D860006133E96821B1439DB38DD3B3D7 (void);
static Il2CppMethodPointer s_methodPointers[64] = 
{
	ARCoreCameraSubsystem_Register_mDDF7E16C611351F9BC5D925C8B536CDE3F90F95D,
	ARCoreCameraSubsystem_CreateProvider_m37D3B9E5E9381A4557973747A59F56F495D1A665,
	ARCoreCameraSubsystem__ctor_mE51F011BF37C360CBDFB4CA00DD190AF271329E5,
	Provider__ctor_m4FE465C11F9431414310E3A4A8F301725BC94A44,
	Provider__cctor_mF801B25F3D1AE886BE57B6217EF9EF88A28B6F16,
	NativeApi_UnityARCore_Camera_Construct_m79D86E9F4D36A18B6B06A8E3E323055D39EE02AB,
	ARCoreFaceRegionData_Equals_m7462EF32B68A814216D80B6F22CA38BBCFB8737A_AdjustorThunk,
	ARCoreFaceRegionData_GetHashCode_mCA630AF2258A0C864182839D6B41B320F8ED9DC0_AdjustorThunk,
	ARCoreFaceRegionData_Equals_mFFB1945FFCB1020D86F83DB2060667DBC90A830D_AdjustorThunk,
	ARCoreFaceRegionData_ToString_m039CBE0D635D1C6C4E704C58BC363D3E2394E7FE_AdjustorThunk,
	ARCoreFaceSubsystem_CreateProvider_m0051EDA00E80118FC9650938DF8464B868E443D8,
	ARCoreFaceSubsystem_RegisterDescriptor_mA6CB48575E3B51FBF4D715631EEB9DF7B2C418F8,
	ARCoreFaceSubsystem__ctor_m8DE7034F95A61AEE431FE4FB9C854DBB9AA7B7C2,
	Provider__ctor_m5255950E187F6D5E7078B7D791567C086AC1DD27,
	ARCoreImageTrackingProvider_RegisterDescriptor_mA50E29E6F9F09ADF1254521E2BFF433D1D21980B,
	ARCoreImageTrackingProvider_CreateProvider_m929BC6984DEBB3271FE73D4355631A5CD4FE3143,
	ARCoreImageTrackingProvider__ctor_m17C6EDEEB185D756FEEB5C4A6AB5EC6FB513D18E,
	ARCoreImageTrackingProvider__cctor_m0DB6BF0658862E8DE668A7C101966D3224A76F0C,
	Provider__ctor_m1AA2B9F3133A6B60C36B687171E4124445D0A8CE,
	ARCorePermissionManager_IsPermissionGranted_m06192CF52A7FC87F26C35620EBF07B7E745B0873,
	ARCorePermissionManager_RequestPermission_mE4D15DE1B58505ACE2F3FC50789DD35FCDCC22B2,
	ARCorePermissionManager_OnPermissionGranted_m4AEF9C4D1F4A87269AC1B452C33209BBC6128940,
	ARCorePermissionManager_OnPermissionDenied_m80AB1413F67AA69B3AFF5C0C51A2B1F5BB3353FB,
	ARCorePermissionManager_OnActivityResult_m441A5092D116700AFDF601F625550CFDDEB34495,
	ARCorePermissionManager__ctor_m3525133DF4FDE7359B1AF77878DCBE7D66867735,
	ARCorePermissionManager_get_instance_mCBE338C3C114B3F52E2EC1615992F294952133B5,
	ARCorePermissionManager_get_activity_m69909D383ADADC460EA1BCE389ADA0712F69E004,
	ARCorePermissionManager_get_permissionsService_m48CB41AA73C9AAAADF94C0C0A7EFD8442A38AEF3,
	ARCorePermissionManager__cctor_mFD32765583188B71172CCBC3BA44F25BFE37B383,
	ARCorePlaneProvider_CreateProvider_mA8C70639859973A8D0F8FD8CD54CD4F2BDD8C08B,
	ARCorePlaneProvider_RegisterDescriptor_m83AC8796A906FB87A486CE712EEFD886CBE472C8,
	ARCorePlaneProvider__ctor_mFAAB1DD1BD53A1B01F26D3F7023C19F8DEA3D97F,
	Provider__ctor_m72BD3537B4C02847A8CF8CC55E03EE3F50802A4F,
	ARCoreRaycastSubsystem_CreateProvider_m5FE3039D0AB6C39BCC9468186FC5739BEF9FF262,
	ARCoreRaycastSubsystem_RegisterDescriptor_m9C14ADD8BB24C067D22A1F7F34C0A24A55F9F565,
	ARCoreRaycastSubsystem__ctor_m843D27D516C0C39D4E3DF17CB4A2C5132B902A1B,
	Provider__ctor_m07868EB509C138E07663A12E5DAC6DA3D8D79FCF,
	ARCoreReferencePointSubsystem_CreateProvider_mE42AA83FA06ED73CA48431E23913EFF6B9AC7E80,
	ARCoreReferencePointSubsystem_RegisterDescriptor_m5BEEAD4CFCB58AECC5E879920CF10E694466A2B0,
	ARCoreReferencePointSubsystem__ctor_m292D29A04B8FD6BA5C86E6326198EFA13BA9397A,
	Provider__ctor_mE4F3BDB17D6AE41525A793DB7B4B0E21981B6633,
	ARCoreSessionSubsystem_CreateProvider_m3E2A8BA6B3C5BB07A6AE4B21834F229AE70745A7,
	ARCoreSessionSubsystem_RegisterDescriptor_mD495549DAA1F4E1765DD584E3A5F34045448787E,
	ARCoreSessionSubsystem__ctor_m2200AD4AACCBC343C9D048A42515CBCB448777EA,
	Provider__ctor_mC9DABA5114D6E7ABB3122F65D7DA76209EF8DBEC,
	Provider_CameraPermissionRequestProvider_mFEC487B73642789DA07336909087B919BDADF1EB,
	U3CU3Ec__cctor_m64E09ED95E8C425402E9B2DD8C8634E162E62E53,
	U3CU3Ec__ctor_mA0A6C55D43B524BE30189C46201EBFD3540B8C07,
	U3CU3Ec__DisplayClass25_0__ctor_m5B8CADE5FA4F4F1927C780A7E21000AC3F8F0F80,
	U3CU3Ec__DisplayClass25_0_U3CCameraPermissionRequestProviderU3Eb__0_m52F0786879125E6E658440ED03143240D68D2522,
	NativeApi_UnityARCore_session_construct_mB49ED374A1D21F55A8BB6F1B2E24473E4AE1F69E,
	NativeApi_UnityARCore_session_getRenderEventFunc_mED022295B10F6DF463E1CF02A5BD5576A955B831,
	CameraPermissionRequestProviderDelegate__ctor_m792EC0BE8D37B67EF22A0F0667E07C2D1CFD3F0A,
	CameraPermissionRequestProviderDelegate_Invoke_m54960A3B1E623735752A53AF5603BFC7F701A049,
	CameraPermissionRequestProviderDelegate_BeginInvoke_mDCF49DB5139A48727879CBC13286D0974D12C698,
	CameraPermissionRequestProviderDelegate_EndInvoke_m3DAFBBEAB7D5A85A049FC5A2C77749E65DE06338,
	CameraPermissionsResultCallbackDelegate__ctor_m5EAA78ACA7411CB386F621E6AB23D2242C3DB770,
	CameraPermissionsResultCallbackDelegate_Invoke_mBDC09109E4DC2472D4505500737052AA9132641F,
	CameraPermissionsResultCallbackDelegate_BeginInvoke_mC45147E135C1EC1C2179503348F715FFD659EC2C,
	CameraPermissionsResultCallbackDelegate_EndInvoke_mC0B26AC474FC40ACBA09400904A4E70D8D41F272,
	ARCoreXRDepthSubsystem_GetInterface_m1964AA9C2CA4E61B72FEB25D0E292DADA9E7C378,
	ARCoreXRDepthSubsystem_RegisterDescriptor_m58189C773B57886BF80869AE81ED9477E7E84C21,
	ARCoreXRDepthSubsystem__ctor_mDFCC3AF83D30FA2DBCFE2AB46C09617892462886,
	Provider__ctor_m6EFC1759D860006133E96821B1439DB38DD3B3D7,
};
static const int32_t s_InvokerIndices[64] = 
{
	3,
	14,
	23,
	23,
	3,
	121,
	1612,
	10,
	9,
	14,
	14,
	3,
	23,
	23,
	3,
	14,
	23,
	3,
	23,
	109,
	122,
	26,
	26,
	23,
	23,
	4,
	4,
	4,
	3,
	14,
	3,
	23,
	23,
	14,
	3,
	23,
	23,
	14,
	3,
	23,
	23,
	14,
	3,
	23,
	26,
	889,
	3,
	23,
	23,
	385,
	111,
	687,
	163,
	163,
	1613,
	26,
	163,
	1614,
	1615,
	26,
	14,
	3,
	23,
	23,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[1] = 
{
	{ 0x0600002E, 1,  (void**)&Provider_CameraPermissionRequestProvider_mFEC487B73642789DA07336909087B919BDADF1EB_RuntimeMethod_var, 0 },
};
extern const Il2CppCodeGenModule g_Unity_XR_ARCoreCodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_ARCoreCodeGenModule = 
{
	"Unity.XR.ARCore.dll",
	64,
	s_methodPointers,
	s_InvokerIndices,
	1,
	s_reversePInvokeIndices,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
